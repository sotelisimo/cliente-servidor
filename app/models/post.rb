class Post < ActiveRecord::Base
    has_many :comments, dependent: :destroy
    validates_presence_of :title
    validates_presence_of :body
    belongs_to :user
    
    validates_presence_of :title
    validates_length_of :title, :in => 5..30, :message => "longitud invalida"
    validates_presence_of :body
    validates :body, length: { maximum: 50, message: "el texto del post es muy largo"}
    validates :body, length: { minimum: 10, message: "el texto del post es corto"}
    
end
